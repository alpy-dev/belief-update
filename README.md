This code is used in the following publication:

Grabisch, Michel, and Muhammed Alperen Yasar. "Frequentist Belief Update Under Ambiguous Evidence in Social Networks." Available at SSRN 4715241 (2024).

You can contact me for a possible error via: muhammedalperenyasar@gmail.com