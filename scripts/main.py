import math
from itertools import chain, combinations
import numpy as np
import pandas as pd
import networkx as nx
import matplotlib.pyplot as plt
import random
import time


def powerset(iter_set):
    s = set(iter_set)
    return chain.from_iterable(combinations(s, r) for r in range(len(s)+1))


class Agent:
    def __init__(self, self_id, initial_belief):
        self.self_id = self_id
        self.belief = initial_belief
        self.evidence = []
        self.age = 0

        self.mass = None
        self.bets = None

    def update_belief(self, power_set, evidence, belief_update_method):
        self.age += 1
        if belief_update_method == "biased":
            self.biased_update(power_set, evidence)
        elif belief_update_method == "combination":
            self.combine_neighbor(power_set, evidence)
        elif belief_update_method == "distributive":
            self.distributed_update(power_set, evidence)
        elif belief_update_method == "unbiased":
            self.unbiased_update(power_set, evidence)
        else:
            raise ValueError("Invalid belief update method")

    def unbiased_update(self, power_set, evidence):
        for i in power_set:
            if evidence == i:
                self.mass[i] = (self.mass[i] * self.age + 1) / (self.age + 1)
            else:
                self.mass[i] = (self.mass[i] * self.age) / (self.age + 1)
#
    def distributed_update(self, power_set, evidence):
        total_weight = 0
        for i in power_set:
            total_weight += self.mass[i]

        for i in power_set:
            if i in evidence:
                self.mass[i] = (self.mass[i] * self.age + self.mass[i] / total_weight) / (self.age + 1)
            else:
                self.mass[i] = (self.mass[i] * self.age) / (self.age + 1)

    def biased_update(self, power_set, evidence):
        if evidence == "a":  # 1 indicates a
            self.mass[1] = (self.mass[1] * self.age + 1) / (self.age + 1)
            self.mass[2] = (self.mass[2] * self.age) / (self.age + 1)
            self.mass[3] = 1 - self.mass[1] - self.mass[2]
        elif evidence == "b":  # 2 indicates b
            self.mass[1] = (self.mass[1] * self.age) / (self.age + 1)
            self.mass[2] = (self.mass[2] * self.age + 1) / (self.age + 1)
            self.mass[3] = 1 - self.mass[1] - self.mass[2]
        else:  # means ab
            self.mass[1] = (self.mass[1] * self.age + self.mass[1] * (1-self.mass[3])) / (self.age + 1)
            self.mass[2] = (self.mass[2] * self.age + self.mass[2] * (1-self.mass[3])) / (self.age + 1)
            self.mass[3] = 1 - self.mass[1] - self.mass[2]
        #
        self.betp_b = self.mass[2] + self.mass[3] / 2

    def combine_neighbor(self, new_mass):

        if (np.isclose(self.mass[1], 1) and np.isclose(new_mass[2], 1)) or \
                (np.isclose(self.mass[2], 1) and np.isclose(new_mass[1], 1)):
            pass
        else:

            conj_e = round((self.mass[1] * new_mass[2] + self.mass[2] * new_mass[1]), 4)
            conj_a = round((self.mass[1] * new_mass[3] + self.mass[1] * new_mass[1] + self.mass[3] * new_mass[1]), 4)
            conj_b = round((self.mass[2] * new_mass[3] + self.mass[2] * new_mass[2] + self.mass[3] * new_mass[2]), 4)
            conj_o = round((self.mass[3] * new_mass[3]), 4)

            inter_mass = np.array([conj_e, conj_a, conj_b, conj_o])
            inter_normalized = inter_mass / np.sum(inter_mass)
            inter_normalized = np.round(inter_normalized, decimals=4)

            normalized_e = 0
            normalized_a = inter_normalized[1] / (1-inter_normalized[0])
            normalized_b = inter_normalized[2] / (1-inter_normalized[0])
            normalized_o = inter_normalized[3] / (1-inter_normalized[0])

            final_mass = np.array([normalized_e, normalized_a, normalized_b, normalized_o])
            final_mass = np.round(final_mass, decimals=4)

            self.mass = final_mass / np.sum(final_mass)

        self.betp_b = self.mass[2] + self.mass[3] / 2

    def bayesian_update(self, new_evidence, p=0.5):
        likelihood = p if new_evidence == 1 else (1 - p)
        self.belief = (likelihood * self.belief) / (likelihood * self.belief + (1 - likelihood) * (1 - self.belief))

    def __str__(self):
        return f"Agent {self.self_id}: {self.belief}"


def normalize_mass(mass):
    new_mass = mass
    for i in range(2):
        if new_mass[i+1] > 0.999:
            new_mass[i+1] = 0.999
            new_mass[int(math.fmod(i+1, 2)+1)] = 0.001
            new_mass[0] = 0
            new_mass[3] = 0
    return new_mass


def get_rand_edges(g, n):
    visited = set()
    results = []
    edges = random.sample(sorted(g.edges()), n)
    for edge in edges:
        if edge[0] in visited or edge[1] in visited:
            continue
        results.append(edge)
        if len(results) == n:
            break
        visited.update(edge)
    return results


class Issue:
    def __init__(self, hypotheses):
        self.hypotheses = hypotheses
        self.len = 2 ^ len(hypotheses)
        self.powerset = list(powerset(self.hypotheses))
        self.nonempties = (1,) * (self.len - 1)
        self.priors = np.random.dirichlet(self.nonempties)#

        if self.len == 2:
            self.true_probs = [0.75, 0.25]
        else:
            self.true_probs = [0.6, 0.2, 0.2]


class SocialNetwork:
    def __init__(self, num_agents, hypotheses, network_type="scale-free"):
        self.num_agents = num_agents
        self.hypotheses = hypotheses
        self.network_type = network_type

        self.issue = Issue(self.hypotheses)
        self.priors = self.issue.priors
        self.powerset = self.issue.powerset

        self.network = self.create_network()
        self.pos = nx.spring_layout(self.network)
        self.agents = self.create_agents()
        self.create_beliefs()
        self.nature_state = self.hypotheses[1]
        self.true_probabilities = self.issue.true_probs

        self.time_table = None

    def create_network(self):
        if self.network_type == "scale-free":
            return nx.barabasi_albert_graph(self.num_agents, m=1)
        elif self.network_type == "small-world":
            return nx.watts_strogatz_graph(self.num_agents, k=2, p=0.1)
        else:
            raise ValueError("Invalid network type")

    def create_agents(self):
        agents = {}
        for node in self.network.nodes:
            # Assign initial beliefs and belief update methods to agents
            initial_belief = np.random.uniform()
            agents[node] = Agent(node, initial_belief)
        return agents

    def create_beliefs(self):
        for agent in self.agents.values():
            agent.mass = self.issue.priors

    def gen_nature_evidence(self):  # p for the set of true probabilities, insert list.
        new_evidence = np.random.choice(self.powerset, p=self.true_probabilities)
        self.nature_state = new_evidence
        return new_evidence

    def pair_agents_and_update_beliefs(self, nature_evidence, pair_percentage=0.1):

        pair_amount = int(pair_percentage * len(self.network.edges))
        random_pairs = get_rand_edges(self.network, pair_amount)
        for pair in random_pairs:

            for agent in range(2):
                if random.random() < self.agents[pair[(agent+1)%2]].mass[1] / \
                        (self.agents[pair[(agent+1)%2]].mass[1] + self.agents[pair[(agent+1)%2]].mass[2]):
                    neighbor_evidence = "a"
                else:
                    neighbor_evidence = "b"

                # self.agents[pair[agent]].update_belief(nature_evidence, "frequentist")
                self.agents[pair[agent]].update_belief(neighbor_evidence, "distributive")
                # self.agents[pair[agent]].update_belief(self.agents[pair[(agent+1)%2]].mass, "combination")

    def run_simulation(self, num_rounds):

        self.time_table = pd.DataFrame(np.zeros((num_rounds, self.num_agents)))

        for agent in self.agents.values():
            print(agent.mass.round(3))

        for t in range(num_rounds):
            turn_evidence = self.gen_nature_evidence()
            self.pair_agents_and_update_beliefs(turn_evidence)

            if t % 1000 == 0:
                print(f"current turn is {t}")

            for node in self.network.nodes:
                self.time_table.iloc[t, node] = self.agents[node].mass[3]

        for agent in self.agents.values():
            print(agent.mass.round(3))

        self.draw_line()

    def draw_network(self):
        plt.figure(figsize=(12, 12))
        nx.draw(self.network, self.pos,
                node_color=[self.agents[node].mass[2] for node in self.network.nodes],
                cmap=plt.colormaps.get_cmap('rainbow'),
                vmin=0,
                vmax=1,
                with_labels=True)

        sm = plt.cm.ScalarMappable(cmap=plt.colormaps.get_cmap('rainbow'),
                                   norm=plt.Normalize(vmin=0, vmax=1))
        sm.set_array([])
        plt.colorbar(sm, shrink=0.8, ax=plt.gca())
        plt.savefig("scale_free.png", dpi=200)
        plt.show()

    def draw_line(self):
        self.time_table.plot(legend=False)
        return

    def analyze_agents(self):
        for agent in self.agents.values():
            print(agent)

    # def study_cluster_behaviors(self):
    #     # Here, I will implement cluster analysis based on the community detection algorithms available in NetworkX
    # pass

    # def draw_circle(self):
    #     plt.figure(figsize=(12, 12))
    #     pos = nx.circular_layout(self.network)
    #     nx.draw(self.network, pos, node_color=[self.agents[node].mass[1] for node in self.network.nodes],
    #             cmap=plt.cm.RdYlBu, with_labels=True)
    #     plt.show()

start = time.time()

h_set = ["l", "c", "r"]
social_network = SocialNetwork(num_agents=200, hypotheses=h_set)
social_network.draw_network()
social_network.run_simulation(num_rounds=500)
social_network.draw_network()

end = time.time()
print(end - start)
