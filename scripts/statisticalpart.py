import random

params = [5, 10, 1]
sample = [random.gammavariate(a, 1) for a in params]
sample = [v / sum(sample) for v in sample]

print(sample)