
import numpy as np
import pandas as pd
import networkx as nx
from networkx.algorithms import community
import matplotlib.pyplot as plt
from itertools import combinations
import random
from netgraph import Graph


def set_node_community(G, communities):
    '''Add community to node attributes'''
    for c, v_c in enumerate(communities):
        for v in v_c:
        # Add 1 to save 0 for external edges
            G.nodes[v]['community'] = c + 1


def set_edge_community(G):
    '''Find internal edges and add their community to their attributes'''
    for v, w, in G.edges:
        if G.nodes[v]['community'] == G.nodes[w]['community']:
        # Internal edge, mark with community
            G.edges[v, w]['community'] = G.nodes[v]['community']
        else:
        # External edge, mark as 0
            G.edges[v, w]['community'] = 0


def get_color(i, r_off=1, g_off=1, b_off=1):
    '''Assign a color to a vertex.'''
    r0, g0, b0 = 0, 0, 0
    n = 16
    low, high = 0.1, 0.9
    span = high - low
    r = low + span * (((i + r_off) * 3) % n) / (n - 1)
    g = low + span * (((i + g_off) * 5) % n) / (n - 1)
    b = low + span * (((i + b_off) * 7) % n) / (n - 1)
    return (r, g, b)


G_fb = nx.read_edgelist("/home/alp/PycharmProjects/belief-update/data/facebook_combined.txt",
                        create_using=nx.Graph(), nodetype=int)
print(G_fb)
# nx.draw(G_fb)
# plt.show()

pos = nx.spring_layout(G_fb)
betCent = nx.betweenness_centrality(G_fb, normalized=True, endpoints=True)
node_color = [20000.0 * G_fb.degree(v) for v in G_fb]
node_size =  [v * 10000 for v in betCent.values()]
plt.figure(figsize=(20,20))
nx.draw_networkx(G_fb, pos=pos, with_labels=False,
node_color=node_color,
node_size=node_size )
plt.axis('off')
plt.show()

nx.draw_networkx(
G_fb,
pos=pos,
node_size=0,
edge_color="#444444",
alpha=0.05,
with_labels=False)
plt.show()

communities = sorted(community.greedy_modularity_communities(G_fb), key=len, reverse=True)
print(len(communities))

plt.rcParams.update(plt.rcParamsDefault)
plt.rcParams.update({'figure.figsize': (15, 10)})
plt.style.use('dark_background')
# Set node and edge communities
set_node_community(G_fb, communities)
set_edge_community(G_fb)
# Set community color for internal edges
external = [(v, w) for v, w in G_fb.edges if G_fb.edges[v, w]['community'] == 0]
internal = [(v, w) for v, w in G_fb.edges if G_fb.edges[v, w]['community'] > 0]
internal_color = ["black" for e in internal]
node_color = [get_color(G_fb.nodes[v]['community']) for v in G_fb.nodes]
# external edges
nx.draw_networkx(
G_fb,
pos=pos,
node_size=0,
edgelist=external,
edge_color="silver",
node_color=node_color,
alpha=0.2,
with_labels=False)
# internal edges
nx.draw_networkx(
G_fb, pos=pos,
edgelist=internal,
edge_color=internal_color,
node_color=node_color,
alpha=0.05,
with_labels=False)
plt.show()
