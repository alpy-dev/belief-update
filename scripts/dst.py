import random


def calculate_mass_assignments(t):
    # Initialize BMA priors for both agents

    m_i_A = 0.6
    m_i_B = 0.3
    m_i_O = 0.1
    m_j_A = 0.1
    m_j_B = 0.5
    m_j_O = 0.4

    for turn in range(1, t+1):
        # Calculate mass assignments for agent i
        if random.random() < m_i_A / (m_i_A + m_i_B):
            e_j = "a"
        else:
            e_j = "b"

        if random.random() < m_j_A / (m_j_A + m_j_B):
            e_i = "a"
        else:
            e_i = "b"

        if e_i == 'a':
            m_i_A = (m_i_A * turn + 1) / (turn + 1)
            m_i_B = (m_i_B * turn) / (turn + 1)
        elif e_i == 'b':
            m_i_A = (m_i_A * turn) / (turn + 1)
            m_i_B = (m_i_B * turn + 1) / (turn + 1)
        m_i_O = (m_i_O * turn) / (turn + 1)

        # Calculate mass assignments for agent j
        if e_j == 'a':
            m_j_A = (m_j_A * turn + 1) / (turn + 1)
            m_j_B = (m_j_B * turn) / (turn + 1)
        elif e_j == 'b':
            m_j_A = (m_j_A * turn) / (turn + 1)
            m_j_B = (m_j_B * turn + 1) / (turn + 1)
        m_j_O = (m_j_O * turn) / (turn + 1)

    return (m_i_A, m_i_B, m_i_O, m_j_A, m_j_B, m_j_O)

final_tuple = calculate_mass_assignments(5)
print(final_tuple)